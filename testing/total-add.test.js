const add = require("./add");
const total = require("./total");
const sum = require("./sum");
describe("Testing total & add fn", () => {
  test("add", () => {
    expect(sum(1, 2)).toBe(3);
  });
  test("total", () => {
    expect(total(5, 20)).toBe("$25");
  });
});
