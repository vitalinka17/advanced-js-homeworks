// <!--Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.-->
//
//     <!--Завдання-->
// <!--Отримати список фільмів серії Зоряні війни та вивести
// на екран список персонажів для кожного з них.-->
//
//     <!--Технічні вимоги:-->
//     <!--Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films
//     та отримати список усіх фільмів серії Зоряні війни-->
// <!--Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі.
//     Список персонажів можна отримати з властивості characters.-->
//     <!--Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані.
//     Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).-->
// <!--Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму,
//     вивести цю інформацію на екран під назвою фільму.-->
//     <!--Необов'язкове завдання підвищеної складності-->
// <!--Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження.
//     Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.-->

const btn = document.querySelector(".btn");
const list = document.querySelector(".list");
const loader = document.querySelector(".lds-roller");
btn.addEventListener("click", clickHandler);

function clickHandler() {
  loader.style.display = "inline-block";
  fetch("https://ajax.test-danit.com/api/swapi/films")
    .then((response) => response.json())
    .then((data) => {
      data.forEach(({ name, episodeId, characters, openingCrawl }) => {
        loader.style.display = "inline-block";
        const li = document.createElement("li");
        const nameEl = document.createElement("span");
        const episodeEl = document.createElement("span");
        const crawlEl = document.createElement("span");
        nameEl.innerText = name;
        episodeEl.innerText = episodeId;
        crawlEl.innerText = openingCrawl;
        li.append(nameEl, episodeEl, crawlEl);
        list.append(li);
        loader.style.display = "none";
        loader.style.display = "inline-block";
        const heroes = characters.map((hero) =>
          fetch(hero).then((resp) => resp.json())
        );
        const persons = Promise.all(heroes);
        const personName = document.createElement("span");
        persons.then((person) => {
          person.forEach(({ name }) => {
            personName.innerText += name;
          });
          loader.style.display = "none";
        });
        li.append(personName);
      });
    })
    .catch((err) => console.warn(err));
}
