// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
//     Технічні вимоги:
//     При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій.
//     Для цього потрібно надіслати GET запит на наступні дві адреси:
//     https://ajax.test-danit.com/api/json/users
//         https://ajax.test-danit.com/api/json/posts
//             Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//     Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x),
//     та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
//     На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
//     При натисканні на неї необхідно надіслати DELETE запит на адресу
//     https://ajax.test-danit.com/api/json/posts/${postId}.
//     Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки,
//     використовуючи JavaScript.
//     Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
//     Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер,
//     не будуть там збережені. Це нормально, все так і має працювати.
//     Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card.
//     При необхідності ви можете додавати також інші класи.
// Необов'язкове завдання підвищеної складності
// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження.
// Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
//     Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно,
//     в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї
//     необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts.
//     Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку).
//     Автором можна присвоїти публікації користувача з id: 1.
//     Додати функціонал (іконку) для редагування вмісту картки.
//     Після редагування картки для підтвердження змін необхідно надіслати
//     PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
const loader = document.querySelector(".lds-roller");
const addTweet = document.getElementById("addTweet");
const form = document.querySelector(".modal");
//
loader.style.display = "inline-block";
addTweet.style.display = "none";
(async () => {
  const posts = await (
    await fetch("https://ajax.test-danit.com/api/json/posts")
  ).json();
  const users = await (
    await fetch("https://ajax.test-danit.com/api/json/users")
  ).json();
  posts.forEach((post) => {
    post.user = users.find((user) => user.id === post.userId);
  });
  new Card(posts).render(".tweet-desk");
  loader.style.display = "none";
  addTweet.style.display = "inline-block";
})();
class Card {
  constructor(posts) {
    this.posts = posts;
  }

  render(selector) {
    this.posts.map(({ id, body, title, user: { name, email, username } }) => {
      document.querySelector(selector).insertAdjacentHTML(
        "beforeend",
        `<div id="tweet_box" class="tweet_box">
          <div id="tweet" class="tweet">
            <div class="head">
              <div class="title">
                <img
                  id="tweet_avatar"
                  src="./assets/avatar.png"
                  alt="avatar"
                  width="48px"
                  height="48px"
                />
                <div class="text">
                  <div class="top">
                    <span class="tweet_name" id="tweet_name">${name}</span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      id="tweet_verified"
                      class="verified hide"
                      width="18"
                      height="18"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                    >
                      <g>
                        <path
                          d="M22.5 12.5c0-1.58-.875-2.95-2.148-3.6.154-.435.238-.905.238-1.4 0-2.21-1.71-3.998-3.818-3.998-.47 0-.92.084-1.336.25C14.818 2.415 13.51 1.5 12 1.5s-2.816.917-3.437 2.25c-.415-.165-.866-.25-1.336-.25-2.11 0-3.818 1.79-3.818 4 0 .494.083.964.237 1.4-1.272.65-2.147 2.018-2.147 3.6 0 1.495.782 2.798 1.942 3.486-.02.17-.032.34-.032.514 0 2.21 1.708 4 3.818 4 .47 0 .92-.086 1.335-.25.62 1.334 1.926 2.25 3.437 2.25 1.512 0 2.818-.916 3.437-2.25.415.163.865.248 1.336.248 2.11 0 3.818-1.79 3.818-4 0-.174-.012-.344-.033-.513 1.158-.687 1.943-1.99 1.943-3.484zm-6.616-3.334l-4.334 6.5c-.145.217-.382.334-.625.334-.143 0-.288-.04-.416-.126l-.115-.094-2.415-2.415c-.293-.293-.293-.768 0-1.06s.768-.294 1.06 0l1.77 1.767 3.825-5.74c.23-.345.696-.436 1.04-.207.346.23.44.696.21 1.04z"
                        ></path>
                      </g>
                    </svg>
                  </div>
                  <div class="bottom">
                    <span id="tweet_username">@${username}</span>
                  </div>
                  <div class="email">
                    <span id="user_email">${email}</span>
                  </div>
                </div>
              </div>
              <div class="dots">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="19"
                  height="19"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                >
                  <g>
                    <circle cx="5" cy="12" r="2"></circle>
                    <circle cx="12" cy="12" r="2"></circle>
                    <circle cx="19" cy="12" r="2"></circle>
                  </g>
                </svg>
              </div>
            </div>
            <div class="content">
            <div contenteditable = "true" id="tweet_title" class="message">${title}</div>
            <div contenteditable = "true" id="tweet_message" class="message">${body}</div>
              <div class="tweet_info">
                <div id="tweet_time">4:38 PM</div>
                &nbsp;&centerdot;&nbsp;
                <div id="tweet_date">Jul 7, 2021</div>
                &nbsp;&centerdot;&nbsp;
                <div id="tweet_client" class="tweet_client">
                  Twitter Web App
                </div>
              </div>
            </div>
            <div class="stats">
              <div class="stat">
                <span id="tweet_retweets" class="count">96</span> Retweets
              </div>
              <div class="stat">
                <span id="tweet_quotes" class="count">88</span> Quote Tweets
              </div>
              <div class="stat">
                <span id="tweet_likes" class="count">153</span> Likes
              </div>
            </div>
            <div class="tail">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <g>
                  <path
                    d="M14.046 2.242l-4.148-.01h-.002c-4.374 0-7.8 3.427-7.8 7.802 0 4.098 3.186 7.206 7.465 7.37v3.828c0 .108.044.286.12.403.142.225.384.347.632.347.138 0 .277-.038.402-.118.264-.168 6.473-4.14 8.088-5.506 1.902-1.61 3.04-3.97 3.043-6.312v-.017c-.006-4.367-3.43-7.787-7.8-7.788zm3.787 12.972c-1.134.96-4.862 3.405-6.772 4.643V16.67c0-.414-.335-.75-.75-.75h-.396c-3.66 0-6.318-2.476-6.318-5.886 0-3.534 2.768-6.302 6.3-6.302l4.147.01h.002c3.532 0 6.3 2.766 6.302 6.296-.003 1.91-.942 3.844-2.514 5.176z"
                  ></path>
                </g>
              </svg>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <g>
                  <path
                    d="M23.77 15.67c-.292-.293-.767-.293-1.06 0l-2.22 2.22V7.65c0-2.068-1.683-3.75-3.75-3.75h-5.85c-.414 0-.75.336-.75.75s.336.75.75.75h5.85c1.24 0 2.25 1.01 2.25 2.25v10.24l-2.22-2.22c-.293-.293-.768-.293-1.06 0s-.294.768 0 1.06l3.5 3.5c.145.147.337.22.53.22s.383-.072.53-.22l3.5-3.5c.294-.292.294-.767 0-1.06zm-10.66 3.28H7.26c-1.24 0-2.25-1.01-2.25-2.25V6.46l2.22 2.22c.148.147.34.22.532.22s.384-.073.53-.22c.293-.293.293-.768 0-1.06l-3.5-3.5c-.293-.294-.768-.294-1.06 0l-3.5 3.5c-.294.292-.294.767 0 1.06s.767.293 1.06 0l2.22-2.22V16.7c0 2.068 1.683 3.75 3.75 3.75h5.85c.414 0 .75-.336.75-.75s-.337-.75-.75-.75z"
                  ></path>
                </g>
              </svg>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <g>
                  <path
                    d="M12 21.638h-.014C9.403 21.59 1.95 14.856 1.95 8.478c0-3.064 2.525-5.754 5.403-5.754 2.29 0 3.83 1.58 4.646 2.73.814-1.148 2.354-2.73 4.645-2.73 2.88 0 5.404 2.69 5.404 5.755 0 6.376-7.454 13.11-10.037 13.157H12zM7.354 4.225c-2.08 0-3.903 1.988-3.903 4.255 0 5.74 7.034 11.596 8.55 11.658 1.518-.062 8.55-5.917 8.55-11.658 0-2.267-1.823-4.255-3.903-4.255-2.528 0-3.94 2.936-3.952 2.965-.23.562-1.156.562-1.387 0-.014-.03-1.425-2.965-3.954-2.965z"
                  ></path>
                </g>
              </svg>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <g>
                  <path
                    d="M17.53 7.47l-5-5c-.293-.293-.768-.293-1.06 0l-5 5c-.294.293-.294.768 0 1.06s.767.294 1.06 0l3.72-3.72V15c0 .414.336.75.75.75s.75-.336.75-.75V4.81l3.72 3.72c.146.147.338.22.53.22s.384-.072.53-.22c.293-.293.293-.767 0-1.06z"
                  ></path>
                  <path
                    d="M19.708 21.944H4.292C3.028 21.944 2 20.916 2 19.652V14c0-.414.336-.75.75-.75s.75.336.75.75v5.652c0 .437.355.792.792.792h15.416c.437 0 .792-.355.792-.792V14c0-.414.336-.75.75-.75s.75.336.75.75v5.652c0 1.264-1.028 2.292-2.292 2.292z"
                  ></path>
                </g>
              </svg>
            </div>
          </div>
          <button type="submit" id="${id}" class="delete__btn" type="button">Delete</button>
        </div>`
      );
    });
    const deleteBtns = document.querySelectorAll(".delete__btn");
    deleteBtns.forEach((btn) => {
      btn.addEventListener("click", () => {
        fetch(`https://ajax.test-danit.com/api/json/posts/${btn.id}`, {
          method: "DELETE",
        })
          .then((res) => res.json())
          .catch((err) => console.log("Error", err));
      });
    });
  }
}
addTweet.addEventListener("click", () => {
  form.classList.toggle("active");
});

form.addEventListener("submit", (e) => {
  e.preventDefault();
  const tweet = {
    title: e.target.title.value,
    body: e.target.message.value,
    userId: 1,
  };
  fetch("https://ajax.test-danit.com/api/json/posts", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(tweet),
  })
    .then((response) => response.json())
    .then((tweet) => {
      (async () => {
        const users = await (
          await fetch("https://ajax.test-danit.com/api/json/users")
        ).json();
        document.querySelector(".tweet-desk").insertAdjacentHTML(
          "afterbegin",
          `<div id="tweet_box" class="tweet_box">
          <div id="tweet" class="tweet">
            <div class="head">
              <div class="title">
                <img
                  id="tweet_avatar"
                  src="./assets/avatar.png"
                  alt="avatar"
                  width="48px"
                  height="48px"
                />
                <div class="text">
                  <div class="top">
                    <span class="tweet_name" id="tweet_name">${users[0].name}</span>
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      id="tweet_verified"
                      class="verified hide"
                      width="18"
                      height="18"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                    >
                      <g>
                        <path
                          d="M22.5 12.5c0-1.58-.875-2.95-2.148-3.6.154-.435.238-.905.238-1.4 0-2.21-1.71-3.998-3.818-3.998-.47 0-.92.084-1.336.25C14.818 2.415 13.51 1.5 12 1.5s-2.816.917-3.437 2.25c-.415-.165-.866-.25-1.336-.25-2.11 0-3.818 1.79-3.818 4 0 .494.083.964.237 1.4-1.272.65-2.147 2.018-2.147 3.6 0 1.495.782 2.798 1.942 3.486-.02.17-.032.34-.032.514 0 2.21 1.708 4 3.818 4 .47 0 .92-.086 1.335-.25.62 1.334 1.926 2.25 3.437 2.25 1.512 0 2.818-.916 3.437-2.25.415.163.865.248 1.336.248 2.11 0 3.818-1.79 3.818-4 0-.174-.012-.344-.033-.513 1.158-.687 1.943-1.99 1.943-3.484zm-6.616-3.334l-4.334 6.5c-.145.217-.382.334-.625.334-.143 0-.288-.04-.416-.126l-.115-.094-2.415-2.415c-.293-.293-.293-.768 0-1.06s.768-.294 1.06 0l1.77 1.767 3.825-5.74c.23-.345.696-.436 1.04-.207.346.23.44.696.21 1.04z"
                        ></path>
                      </g>
                    </svg>
                  </div>
                  <div class="bottom">
                    <span id="tweet_username">@${users[0].username}</span>
                  </div>
                  <div class="email">
                    <span id="user_email">${users[0].email}</span>
                  </div>
                </div>
              </div>
              <div class="dots">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="19"
                  height="19"
                  viewBox="0 0 24 24"
                  fill="currentColor"
                >
                  <g>
                    <circle cx="5" cy="12" r="2"></circle>
                    <circle cx="12" cy="12" r="2"></circle>
                    <circle cx="19" cy="12" r="2"></circle>
                  </g>
                </svg>
              </div>
            </div>
            <div class="content">
            <div id="tweet_title" class="message">${tweet.title}</div>
            <div id="tweet_message" class="message">${tweet.body}</div>
              <div class="tweet_info">
                <div id="tweet_time">4:38 PM</div>
                &nbsp;&centerdot;&nbsp;
                <div id="tweet_date">Jul 7, 2021</div>
                &nbsp;&centerdot;&nbsp;
                <div id="tweet_client" class="tweet_client">
                  Twitter Web App
                </div>
              </div>
            </div>
            <div class="stats">
              <div class="stat">
                <span id="tweet_retweets" class="count">96</span> Retweets
              </div>
              <div class="stat">
                <span id="tweet_quotes" class="count">88</span> Quote Tweets
              </div>
              <div class="stat">
                <span id="tweet_likes" class="count">153</span> Likes
              </div>
            </div>
            <div class="tail">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <g>
                  <path
                    d="M14.046 2.242l-4.148-.01h-.002c-4.374 0-7.8 3.427-7.8 7.802 0 4.098 3.186 7.206 7.465 7.37v3.828c0 .108.044.286.12.403.142.225.384.347.632.347.138 0 .277-.038.402-.118.264-.168 6.473-4.14 8.088-5.506 1.902-1.61 3.04-3.97 3.043-6.312v-.017c-.006-4.367-3.43-7.787-7.8-7.788zm3.787 12.972c-1.134.96-4.862 3.405-6.772 4.643V16.67c0-.414-.335-.75-.75-.75h-.396c-3.66 0-6.318-2.476-6.318-5.886 0-3.534 2.768-6.302 6.3-6.302l4.147.01h.002c3.532 0 6.3 2.766 6.302 6.296-.003 1.91-.942 3.844-2.514 5.176z"
                  ></path>
                </g>
              </svg>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <g>
                  <path
                    d="M23.77 15.67c-.292-.293-.767-.293-1.06 0l-2.22 2.22V7.65c0-2.068-1.683-3.75-3.75-3.75h-5.85c-.414 0-.75.336-.75.75s.336.75.75.75h5.85c1.24 0 2.25 1.01 2.25 2.25v10.24l-2.22-2.22c-.293-.293-.768-.293-1.06 0s-.294.768 0 1.06l3.5 3.5c.145.147.337.22.53.22s.383-.072.53-.22l3.5-3.5c.294-.292.294-.767 0-1.06zm-10.66 3.28H7.26c-1.24 0-2.25-1.01-2.25-2.25V6.46l2.22 2.22c.148.147.34.22.532.22s.384-.073.53-.22c.293-.293.293-.768 0-1.06l-3.5-3.5c-.293-.294-.768-.294-1.06 0l-3.5 3.5c-.294.292-.294.767 0 1.06s.767.293 1.06 0l2.22-2.22V16.7c0 2.068 1.683 3.75 3.75 3.75h5.85c.414 0 .75-.336.75-.75s-.337-.75-.75-.75z"
                  ></path>
                </g>
              </svg>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <g>
                  <path
                    d="M12 21.638h-.014C9.403 21.59 1.95 14.856 1.95 8.478c0-3.064 2.525-5.754 5.403-5.754 2.29 0 3.83 1.58 4.646 2.73.814-1.148 2.354-2.73 4.645-2.73 2.88 0 5.404 2.69 5.404 5.755 0 6.376-7.454 13.11-10.037 13.157H12zM7.354 4.225c-2.08 0-3.903 1.988-3.903 4.255 0 5.74 7.034 11.596 8.55 11.658 1.518-.062 8.55-5.917 8.55-11.658 0-2.267-1.823-4.255-3.903-4.255-2.528 0-3.94 2.936-3.952 2.965-.23.562-1.156.562-1.387 0-.014-.03-1.425-2.965-3.954-2.965z"
                  ></path>
                </g>
              </svg>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="22"
                viewBox="0 0 24 24"
                fill="currentColor"
              >
                <g>
                  <path
                    d="M17.53 7.47l-5-5c-.293-.293-.768-.293-1.06 0l-5 5c-.294.293-.294.768 0 1.06s.767.294 1.06 0l3.72-3.72V15c0 .414.336.75.75.75s.75-.336.75-.75V4.81l3.72 3.72c.146.147.338.22.53.22s.384-.072.53-.22c.293-.293.293-.767 0-1.06z"
                  ></path>
                  <path
                    d="M19.708 21.944H4.292C3.028 21.944 2 20.916 2 19.652V14c0-.414.336-.75.75-.75s.75.336.75.75v5.652c0 .437.355.792.792.792h15.416c.437 0 .792-.355.792-.792V14c0-.414.336-.75.75-.75s.75.336.75.75v5.652c0 1.264-1.028 2.292-2.292 2.292z"
                  ></path>
                </g>
              </svg>
            </div>
          </div>
          <button type="submit" id="delete" class="delete__btn" type="button">Delete</button>
        </div>`
        );
        form.classList.remove("active");
      })();
    })
    .catch((error) => {
      console.error("Error:", error);
    });
});
