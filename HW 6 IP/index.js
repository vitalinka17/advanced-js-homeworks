// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
// Це тоді коли операції виконуються незалежно від інших процесів.
// Це дозволяє двигуну JS виконувати ці асинхронні операції в той час,
// коли синхронні також виконуються без додаткових ставань в стек чергу.

// Завдання
// Написати програму "Я тебе знайду по IP"
//
// Технічні вимоги:
//     Створити просту HTML-сторінку з кнопкою Знайти по IP.
//     Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json,
//     отримати звідти IP адресу клієнта.
//     Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/
//     та отримати інформацію про фізичну адресу.
//     під кнопкою вивести на сторінку інформацію, отриману з останнього запиту –
//     континент, країна, регіон, місто, район.
//     Усі запити на сервер необхідно виконати за допомогою async await.
const findBtn = document.getElementById("find");
const USER_API = "https://api.ipify.org/?format=json";
const list = document.querySelector(".list");
async function getUserInfo() {
  let response = await fetch(USER_API);
  let data = await response.json();
  return data;
}
async function getUserIpInfo(ip) {
  let response = await fetch(`http://ip-api.com/json/${ip}?fields=1581081`);
  let data = await response.json();
  return data;
}
function render(obj, place, item) {
  obj[item]
    ? list.insertAdjacentHTML(
        "beforeend",
        `<span class=list__item>${obj[item]}</span>`
      )
    : list.insertAdjacentHTML(
        "beforeend",
        `<span class=list__item>${item} hasn't been found by the program</span>`
      );
}
function fetchUserInfo() {
  getUserInfo().then(({ ip }) =>
    getUserIpInfo(ip).then(
      ({ continent, country, regionName, city, district }) => {
        let obj = {
          continent,
          country,
          regionName,
          city,
          district,
        };
        Object.keys(obj).forEach((item) => {
          render(obj, list, item);
        });
      }
    )
  );
}

findBtn.addEventListener("click", fetchUserInfo);
