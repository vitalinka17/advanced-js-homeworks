// Створити поле 10*10 за допомогою елемента <table>.
// Суть гри: будь-яка непідсвічена комірка в таблиці на короткий час забарвлюється в синій колір.
// Користувач повинен протягом відведеного часу встигнути клацнути на зафарбовану комірку.
// Якщо користувач встиг це зробити, вона забарвлюється зелений колір, користувач отримує 1 очко.
// Якщо не встиг – вона забарвлюється у червоний колір, комп'ютер отримує 1 очко.
// Гра триває доти, доки половина комірок на полі не будуть пофарбовані у зелений чи червоний колір.
// Як тільки це станеться, той гравець (людина чи комп'ютер), чиїх комірок на полі більше, перемагає.
// Гра повинна мати три рівні складності, що вибираються перед стартом гри:
//     Легкий – нова комірка підсвічується кожні 1.5 секунди;
// Середній - нова комірка підсвічується раз на секунду;
// Важкий - нова комірка підсвічується кожні півсекунди.
//     Після закінчення гри вивести на екран повідомлення про те, хто переміг.
//     Після закінчення гри має бути можливість змінити рівень складності та розпочати нову гру.
//     Використовувати функціонал ООП під час написання програми.
//     За бажанням, замість фарбування комірок кольором, можна вставляти туди картинки.

const colors = {
  highlightColor: "rgb(37, 150, 190)",
  clickedColor: "rgb(0, 153, 0)",
  unClickedColor: "rgb(230, 0, 0)",
};

const { highlightColor, clickedColor, unClickedColor } = colors;

const buttons = document.querySelector(".buttons");
const playerCounter = document.querySelector(".player");
let playerScore = 0;
const computerCounter = document.querySelector(".computer");
let computerScore = 0;
let winner = null;
const table = document.querySelector(".table");
const cells = document.getElementsByTagName("td");
class Game {
  constructor(countCells) {
    this.usedNums = [];
    this.countCells = countCells;
    this.halfOfCells = this.countCells / 2;
  }
  setUsedNum(number) {
    this.usedNums.push(number);
  }
  isStopGame() {
    return this.usedNums.length >= this.halfOfCells;
  }
  cellRandomiser() {
    return Math.floor(Math.random() * this.countCells);
  }
  checkForUsedCell(number) {
    return this.usedNums.includes(number);
  }
  clearUsedNums() {
    this.usedNums = [];
  }
}
const whackAMole = new Game(cells.length);

buttons.addEventListener("click", btnClickHandler);
function btnClickHandler(event) {
  if (event.target.tagName.toLowerCase() === "button") {
    if (event.target.className === "easyBtn") {
      gameMode(2500);
    } else if (event.target.className === "mediumBtn") {
      gameMode(1000);
    } else if (event.target.className === "hardBtn") {
      gameMode(500);
    }
  } else return false;
}

function gameMode(timeout) {
  const interval = setInterval(() => {
    if (whackAMole.isStopGame()) {
      clearInterval(interval);
      Array.from(cells).forEach((item) => {
        item.style.backgroundColor = "white";
      });
      computerScore = 0;
      playerScore = 0;
      computerCounter.innerText = 0;
      playerCounter.innerText = 0;
      whackAMole.clearUsedNums();
      alert(`Game over, the winner is ${winner}`);
      winner = null;
      return;
    }
    const random = whackAMole.cellRandomiser();

    if (whackAMole.checkForUsedCell(random)) {
      clearInterval(interval);
      gameMode(timeout);
      return;
    }

    whackAMole.setUsedNum(random);

    const cell = cells[random];

    cell.style.backgroundColor = highlightColor;

    const handlerClick = () => {
      cell.style.backgroundColor = clickedColor;
    };

    cell.addEventListener("click", handlerClick);

    setTimeout(() => {
      if (cell.style.backgroundColor !== clickedColor) {
        cell.style.backgroundColor = unClickedColor;
      }
      cell.removeEventListener("click", handlerClick);
      if (cell.style.backgroundColor === clickedColor) {
        playerScore++;
        playerCounter.innerText = playerScore;
      } else {
        computerScore++;
        computerCounter.innerText = computerScore;
      }
      playerScore > computerScore
        ? (winner = "YOU, congrats!)")
        : (winner = "Computer, next time will be you:)");
    }, timeout / 2);
  }, timeout);
}
